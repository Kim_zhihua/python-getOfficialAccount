from pprint import pprint
import json
from public_article_test import public_article
from public_search_test import public_search_api


def add_info_article(public_article_list, public_info):
    public_article_info_list = list()
    for public_article in public_article_list:
        item = dict()
        item["article"] = public_article
        item['gzh'] = public_info
        public_article_info_list.append(item)
    return public_article_info_list


def save_info_to_file(article_info, author_info):
    file_name = author_info['public_name'] + "的文章"
    with open('{}.json'.format(file_name), 'a+', encoding='utf8') as f:
        f.write(json.dumps(article_info, ensure_ascii=False, indent=4))


def process_console():
    public_name = input("请输入要查找的公众号：")
    public_info = public_search_api(public_name)
    print("公众号信息：")
    pprint(public_info)
    num = input("是否查询该作者的文章：1>是 2>否：")
    if num == "1":
        public_article_list = public_article(public_info['public_name'])
        public_article_list = add_info_article(public_article_list, public_info)
        save_info_to_file(public_article_list, public_info)
        print("已写入当前目录中：{}.json".format(public_info['public_name']))
    else:
        print("欢迎再次使用")


if __name__ == '__main__':
    process_console()
